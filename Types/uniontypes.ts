module uniontypes{
    function padLeft(value: string, padding: any) {
        if (typeof padding === "number") {
            return Array(padding + 1).join(" ") + value;
        }
        if (typeof padding === "string") {
            return padding + value;
        }
        throw new Error(`Expected string or number, got '${padding}'.`);
    }

    padLeft("Hello world", 4)

    padLeft("Oh my", true);
    

    function padLeft2(value: string, padding: string | number) {
        if (typeof padding === "number") {
            return Array(padding + 1).join(" ") + value;
        }
        if (typeof padding === "string") {
            return padding + value;
        }
        throw new Error(`Expected string or number, got '${padding}'.`);
    }

    padLeft2("Hello World", "Oh My");
    padLeft2("Hello world", 34);
    //padLeft2("Hello world", true);
}