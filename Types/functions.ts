// namespace is the preferred keyword over module
namespace functions {

    // Demo A
    // Type the parameters
    let squareItSimple = function (h: number, w: number) {
        return h * w;
    };

    let squareItSimplest = (h: number, w: number) => h * w;

    let squareItSimplest2 = (h: number, w: number) => {
        if (w == undefined) {
            return h * h;
        }

        return h * w;
    };

    console.log('squareItSimple = ' + squareItSimple(7, 12));
    console.log('squareItSimplest = ' + squareItSimplest(7, 12));

    // Demo B
    // Arrow functions and returning void
    let helloWorld: (name?: string) => void;
    helloWorld = (name?: string) => {
        console.log('Hello ' + (name || ' unknown person'));
    }
    helloWorld('Who');
    helloWorld();

    let helloWorldRequired = (name: string) => {
        console.log("Hello" + name);
    }

    helloWorldRequired("John");

    let helloWorldOptional = (age?: number, name?: string) => {

    }

    helloWorldOptional();

    let helloWorldDefault = (age: number = 10, name: string = "Unknown") => {

    }

    helloWorldDefault();

    let helloWorldDefaultAndOptional = (age: number=10, name? : string)=>{
    }
    
    helloWorldDefaultAndOptional(12);



    // Pass an object literal as the parameter and use arrow functions
    // Demo C
    let squareIt: (rect: { h: number; w?: number; }) => number;

    let rectA = { h: 7 };
    let rectB = { h: 7, w: 12 };

    squareIt = function (rect) {
        if (rect.w === undefined) {
            return rect.h * rect.h;
        }
        return rect.h * rect.w;
    };

    let val2: number = squareIt(rectA);
    console.log('rectangle h and w of 7 = ' + val2);

    let val3: number = squareIt(rectB);
    console.log('rectangle h of 7 and width of 12 = ' + val3);
}