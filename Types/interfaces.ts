// namespace is the preferred keyword over module
namespace interfaces {

    // Simple arrow function demo from the slides
    let greetMe: (msg: string) => void;
    greetMe = function (msg) {
        console.log(msg);
    }
    greetMe('Hello!');

    let greetMeAdvanced: (msg1: string, msg2?: string) => {

    }



    // Demo A
    // Use interfaces
    interface SquaringFunction {
        (x: number): number;
    }

    let squareItBasic: SquaringFunction
        = (num) => num * num;

    let squareItAdvance: SquaringFunction = (num) => num ^ 2;

    console.log('Square of 12 = ' + squareItBasic(12));
    console.log("Advanced Sqaure" + squareItAdvance(12));

    // Demo B
    // Pass an object literal as the parameter and use arrow functions
    interface Rectangle {
        h: number;
        w?: number;
    }

    //let squareIt: (rect: { h: number; w?: number; }) => number;
    let squareIt: (rect: Rectangle) => number;

    let rectA: Rectangle = { h: 7 };
    let rectB: Rectangle = { h: 7, w: 12 };

    let newRect: Rectangle = { h: 10, w: 20 }

    squareIt = function (rect) {
        if (rect.w === undefined) {
            return rect.h * rect.h;
        }
        return rect.h * rect.w;
    };

    let val1: number = squareIt(rectA);
    console.log('rectangle h and w of 7 = ' + val1);

    let val2: number = squareIt(rectB);
    console.log('rectangle h of 7 and width of 12 = ' + val2);

    // Demo C
    // Type the function, the parameter, and use arrow functions
    // Learn more about interfaces in Module 3
    interface Person {
        name: string;
        age?: number;
        kids: number;
        calcPets: () => number;
        makeYounger: (years: number) => void;
        greet: (msg: string) => string;
    }

    let p: Person = {
        name: 'Who',
        age: 40,
        kids: 4,
        calcPets: function () {
            return this.kids * 2;
        },
        makeYounger: function (years: number) {
            this.age -= years;
        },
        greet: function (msg: string) {
            return msg + ', ' + this.name;
        }
    };

    let pets = p.calcPets();
    console.log('pets = ' + pets);

    p.makeYounger(10);
    let newAge = p.age;
    console.log('new age = ' + newAge);

    let msg = p.greet('Good day');
    console.log(msg);


    // Demo D
    // Returning an interface from a function
    interface SessionEval {
        addRating: (rating: number) => void;
        calcRating: () => number;
    }

    function sessionEvaluator(): SessionEval {
        let ratings: number[] = [];
        let addRating = (rating: number = 5) =>
            ratings.push(rating);
        ;
        let calcRating = () => {
            let sum: number = 0;
            ratings.forEach(function (score) {
                sum += score;
            });

            return sum / ratings.length;
        };

        return {
            addRating: addRating,
            calcRating: calcRating
        }
    }

    let s = sessionEvaluator();
    s.addRating(4);
    s.addRating(5);
    s.addRating(5);
    s.addRating(5);
    console.log(s.calcRating());
}