namespace myNamespace {
    // any
    let data: any;
    let info;
    let doSomething = function (arg: number) {
        return arg
    };
    let val = doSomething(2);

    // primitives
    let age: number = 2;
    let score: number = 98.25;
    let rating = 98.25;

    let hasData: boolean = true;
    let isReady = true;
    let isBald = function () { return true; };
    let hasHair = !!isBald();

    let firstName: string = 'Some';
    let lastName = 'Body';

    // string array
    function getArrayLength(x: string[]) {
        let len = x.length;
        return len;
    }

    let names: string[] = ['Johnw', 'Dan', 'Aaron', 'Fritz'];

    let firstPerson: string = names[0];

    console.log(getArrayLength(names))


    // null
    let guitarSales: any = null;
    let animal = null;
    let orderDate: Date = null;
    let num: number = null;
    let str: string = null;
    let isHappy: boolean = null;
    let customer: {} = null;


    // undefined
    let quantity: number;
    let company = undefined;
    console.log('undefined examples');
    console.log(quantity);
    console.log(company);
}