module enums {
    enum Direction {
        Up =2,
        Down,
        Left,
        Right,
    }

    let currentDirection: Direction = Direction.Down;

    enum Direction2 {
        Up = "UP",
        Down = "DOWN",
        Left = "LEFT",
        Right = "RIGHT",
    }

    let anotherDirection: Direction2 = Direction2.Down;

}