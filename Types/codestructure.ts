namespace MyModule {
    interface myInterface {

    }

    class myClass implements myInterface {
        private field1: string;
        public field2: number;
        constructor() {

        }

        method1() {

        }

        method2(): number {
            return 1;
        }

        get prop1() {
            return this.field1;
        }
        set prop1(value) {
            this.field1= value;
        }
    }

    let myObj = new myClass();
    myObj.field2 = 100;
    myObj.prop1 = "value";
}