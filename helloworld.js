var MyClass = /** @class */ (function () {
    function MyClass(name) {
        this.name = name;
        console.log("I am getting created");
    }
    MyClass.prototype.print = function () {
        console.log(this.name);
    };
    return MyClass;
}());
var obj = new MyClass("Hello world");
obj.print();
