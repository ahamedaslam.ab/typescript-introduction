module generictypes {
    function identity<T>(arg: T): T {
        return arg;
    }
    
    let myIdentity: <T>(arg: T) => T = identity;

    myIdentity<string>("");

    function identity2<T>(arg: T): T {
        return arg;
    }
    
    let myIdentity2: <U>(arg: U) => U = identity
}