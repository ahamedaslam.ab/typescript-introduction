module generics {
    function identity(arg: number): number {
        return arg;
    }

    function identity2(arg: any): any {
        return arg;
    }

    identity2("test")

    function identity5(arg: number| string| boolean): number | string| boolean{
        return arg;
    }

    function identity3<T>(arg: T): T {
        return arg;
    }

    identity3<number>(10);

    identity3<string>("sample");

    function identity4<T>(arg: T): T {
        return arg;
    }

    function loggingIdentity<T>(arg: T): T {
        //console.log(arg.length);  // Error: T doesn't have .length
        return arg;
    }

    function loggingIdentity2<T>(arg: T[]): T[] {
        console.log(arg.length);  // Array has a .length, so no more error
        return arg;
    }

    loggingIdentity2<string>(["1","2","3"]);
}