module genericconstaints {

    function loggingIdentity<T>(arg: T): T {
        //console.log(arg.length);  // Error: T doesn't have .length
        return arg;
    }

    interface Lengthwise {
        length: number;
    }
    
    function loggingIdentity2<T extends Lengthwise>(arg: T): T {
        console.log(arg.length);  // Now we know it has a .length property, so no more error
        return arg;
    }

    //loggingIdentity2(3);  // Error, number doesn't have a .length property

    //loggingIdentity2<number>(10);

    class MyClass implements Lengthwise{
         constructor(public length: number){             
         }
    }

    loggingIdentity2({length: 10, value: 3});

    loggingIdentity2(new MyClass(10));

    class AdvancedGeneric<T1, T2>{
        value1: T1;
        value2: T2;
    }

    var obj = new AdvancedGeneric<number, string>();
    obj.value1 = 10;
    obj.value2 = "10";



    function getProperty<T, K extends keyof T>(obj: T, key: K) {
        return obj[key];
    }
    
    let x = { a: 1, b: 2, c: 3, d: 4 };
    
    getProperty(x, "a"); // okay
    //getProperty(x, "m");
}