// namespace is the preferred keyword over module
var interfaces;
(function (interfaces) {
    // Simple arrow function demo from the slides
    let greetMe;
    greetMe = function (msg) {
        console.log(msg);
    };
    greetMe('Hello!');
    let greetMeAdvanced;
    let squareItBasic = (num) => num * num;
    let squareItAdvance = (num) => num ^ 2;
    console.log('Square of 12 = ' + squareItBasic(12));
    console.log("Advanced Sqaure" + squareItAdvance(12));
    //let squareIt: (rect: { h: number; w?: number; }) => number;
    let squareIt;
    let rectA = { h: 7 };
    let rectB = { h: 7, w: 12 };
    let newRect = { h: 10, w: 20 };
    squareIt = function (rect) {
        if (rect.w === undefined) {
            return rect.h * rect.h;
        }
        return rect.h * rect.w;
    };
    let val1 = squareIt(rectA);
    console.log('rectangle h and w of 7 = ' + val1);
    let val2 = squareIt(rectB);
    console.log('rectangle h of 7 and width of 12 = ' + val2);
    let p = {
        name: 'Who',
        age: 40,
        kids: 4,
        calcPets: function () {
            return this.kids * 2;
        },
        makeYounger: function (years) {
            this.age -= years;
        },
        greet: function (msg) {
            return msg + ', ' + this.name;
        }
    };
    let pets = p.calcPets();
    console.log('pets = ' + pets);
    p.makeYounger(10);
    let newAge = p.age;
    console.log('new age = ' + newAge);
    let msg = p.greet('Good day');
    console.log(msg);
    function sessionEvaluator() {
        let ratings = [];
        let addRating = (rating = 5) => ratings.push(rating);
        ;
        let calcRating = () => {
            let sum = 0;
            ratings.forEach(function (score) {
                sum += score;
            });
            return sum / ratings.length;
        };
        return {
            addRating: addRating,
            calcRating: calcRating
        };
    }
    let s = sessionEvaluator();
    s.addRating(4);
    s.addRating(5);
    s.addRating(5);
    s.addRating(5);
    console.log(s.calcRating());
})(interfaces || (interfaces = {}));
//# sourceMappingURL=interfaces.js.map