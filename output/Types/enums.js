var enums;
(function (enums) {
    let Direction;
    (function (Direction) {
        Direction[Direction["Up"] = 2] = "Up";
        Direction[Direction["Down"] = 3] = "Down";
        Direction[Direction["Left"] = 4] = "Left";
        Direction[Direction["Right"] = 5] = "Right";
    })(Direction || (Direction = {}));
    let currentDirection = Direction.Down;
    let Direction2;
    (function (Direction2) {
        Direction2["Up"] = "UP";
        Direction2["Down"] = "DOWN";
        Direction2["Left"] = "LEFT";
        Direction2["Right"] = "RIGHT";
    })(Direction2 || (Direction2 = {}));
    let anotherDirection = Direction2.Down;
})(enums || (enums = {}));
//# sourceMappingURL=enums.js.map