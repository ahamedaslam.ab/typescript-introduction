var myNamespace;
(function (myNamespace) {
    // any
    let data;
    let info;
    let doSomething = function (arg) {
        return arg;
    };
    let val = doSomething(2);
    // primitives
    let age = 2;
    let score = 98.25;
    let rating = 98.25;
    let hasData = true;
    let isReady = true;
    let isBald = function () { return true; };
    let hasHair = !!isBald();
    let firstName = 'Some';
    let lastName = 'Body';
    // string array
    function getArrayLength(x) {
        let len = x.length;
        return len;
    }
    let names = ['Johnw', 'Dan', 'Aaron', 'Fritz'];
    let firstPerson = names[0];
    console.log(getArrayLength(names));
    // null
    let guitarSales = null;
    let animal = null;
    let orderDate = null;
    let num = null;
    let str = null;
    let isHappy = null;
    let customer = null;
    // undefined
    let quantity;
    let company = undefined;
    console.log('undefined examples');
    console.log(quantity);
    console.log(company);
})(myNamespace || (myNamespace = {}));
//# sourceMappingURL=primitives.js.map