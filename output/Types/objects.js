// namespace is the preferred keyword over module
var objects;
(function (objects) {
    // Object
    let points1 = { x: 10, y: 20 };
    let points2;
    points2 = { x: 10, y: 20 };
    let points3 = { x: 10, y: 20 };
    points3 = { foo: 'john' };
    let rectangle = {
        h: 10,
        w: 20,
        calcArea: function () {
            return this.h * this.w;
        }
    };
    console.log('rectangle area = ' + rectangle.calcArea());
    // Functions
    let squareIt1 = function (x) {
        return x * x;
    };
    let val1 = squareIt1('2');
    console.log('squareIt1 = ' + val1);
    val1 = squareIt1('John');
    console.log('squareIt1 = ' + val1);
    // Type the parameter
    let squareIt2 = function (x) {
        return x * x;
    };
    //let val2 = squareIt2('John');
    //let val2 = squareIt2('4');
    let val2 = squareIt2(4);
    console.log('squareIt2 = ' + val2);
    let squareIt3;
    squareIt3 = function (x) {
        return x * x;
    };
    let val3 = squareIt3(8);
    console.log('squareIt3 = ' + val3);
    let squareIt = function (rect) {
        if (rect.w === undefined) {
            return rect.h * rect.h;
        }
        return rect.h * rect.w;
    };
    let sq1 = squareIt({ h: 10 });
    console.log('rectangle h and w of 10 = ' + sq1);
    let sq2 = squareIt({ h: 10, w: 40 });
    console.log('rectangle h of 10 and width of 40 = ' + sq2);
})(objects || (objects = {}));
//# sourceMappingURL=objects.js.map