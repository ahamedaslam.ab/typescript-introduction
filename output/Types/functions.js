// namespace is the preferred keyword over module
var functions;
(function (functions) {
    // Demo A
    // Type the parameters
    let squareItSimple = function (h, w) {
        return h * w;
    };
    let squareItSimplest = (h, w) => h * w;
    let squareItSimplest2 = (h, w) => {
        if (w == undefined) {
            return h * h;
        }
        return h * w;
    };
    console.log('squareItSimple = ' + squareItSimple(7, 12));
    console.log('squareItSimplest = ' + squareItSimplest(7, 12));
    // Demo B
    // Arrow functions and returning void
    let helloWorld;
    helloWorld = (name) => {
        console.log('Hello ' + (name || ' unknown person'));
    };
    helloWorld('Who');
    helloWorld();
    let helloWorldRequired = (name) => {
        console.log("Hello" + name);
    };
    helloWorldRequired("John");
    let helloWorldOptional = (age, name) => {
    };
    helloWorldOptional();
    let helloWorldDefault = (age = 10, name = "Unknown") => {
    };
    helloWorldDefault();
    let helloWorldDefaultAndOptional = (age = 10, name) => {
    };
    helloWorldDefaultAndOptional(12);
    // Pass an object literal as the parameter and use arrow functions
    // Demo C
    let squareIt;
    let rectA = { h: 7 };
    let rectB = { h: 7, w: 12 };
    squareIt = function (rect) {
        if (rect.w === undefined) {
            return rect.h * rect.h;
        }
        return rect.h * rect.w;
    };
    let val2 = squareIt(rectA);
    console.log('rectangle h and w of 7 = ' + val2);
    let val3 = squareIt(rectB);
    console.log('rectangle h of 7 and width of 12 = ' + val3);
})(functions || (functions = {}));
//# sourceMappingURL=functions.js.map