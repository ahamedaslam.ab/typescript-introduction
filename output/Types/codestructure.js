var MyModule;
(function (MyModule) {
    class myClass {
        constructor() {
        }
        method1() {
        }
        method2() {
            return 1;
        }
        get prop1() {
            return this.field1;
        }
        set prop1(value) {
            this.field1 = value;
        }
    }
    let myObj = new myClass();
    myObj.field2 = 100;
    myObj.prop1 = "value";
})(MyModule || (MyModule = {}));
//# sourceMappingURL=codestructure.js.map