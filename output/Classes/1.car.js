class Engine {
    constructor(horsePower, engineType) {
        this.horsePower = horsePower;
        this.engineType = engineType;
    }
}
class OlderEngine {
    constructor() {
        let newEngine = new Engine(300, "Single Cylinder");
        let olderEngine = new OlderEngine();
        olderEngine.engineType = "No Cyclinder";
        olderEngine.horsePower = 50;
    }
}
class Car {
    constructor(engine) {
        this.engine = engine;
    }
    get engine() {
        return this._engine;
    }
    set engine(value) {
        if (value == undefined)
            throw 'Please supply an engine';
        this._engine = value;
    }
    start() {
        console.log('Car engine started ' + this._engine.engineType);
    }
}
let engine = new Engine(300, 'V8');
let car = new Car(engine);
var i20Engine = new Engine(1000, "i20");
car.engine = i20Engine;
console.log(car.engine.engineType);
car.start();
//# sourceMappingURL=1.car.js.map