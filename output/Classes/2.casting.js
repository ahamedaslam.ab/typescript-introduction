class Dog {
    bark() {
        console.log("barked");
    }
}
class Cat {
    yowl() {
        console.log("Yowl");
    }
}
class Me {
    constructor(pet) {
        this.pet = pet;
    }
    doTrick() {
        if (this.pet instanceof Dog) {
            //this.pet.bark();
            let dog = this.pet;
            dog.bark();
        }
        else {
            let cat = this.pet;
            cat.yowl();
        }
    }
}
//# sourceMappingURL=2.casting.js.map