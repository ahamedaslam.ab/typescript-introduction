var genericconstaints;
(function (genericconstaints) {
    function loggingIdentity(arg) {
        //console.log(arg.length);  // Error: T doesn't have .length
        return arg;
    }
    function loggingIdentity2(arg) {
        console.log(arg.length); // Now we know it has a .length property, so no more error
        return arg;
    }
    //loggingIdentity2(3);  // Error, number doesn't have a .length property
    loggingIdentity2({ length: 10, value: 3 });
    function getProperty(obj, key) {
        return obj[key];
    }
    let x = { a: 1, b: 2, c: 3, d: 4 };
    getProperty(x, "a"); // okay
    //getProperty(x, "m");
})(genericconstaints || (genericconstaints = {}));
//# sourceMappingURL=4.genericconstraints.js.map