var classtypes;
(function (classtypes) {
    function create(c) {
        return new c();
    }
    class BeeKeeper {
    }
    class ZooKeeper {
    }
    class Animal {
    }
    class Bee extends Animal {
    }
    class Lion extends Animal {
    }
    function createInstance(c) {
        return new c();
    }
    createInstance(Lion).keeper.nametag; // typechecks!
    createInstance(Bee).keeper.hasMask; // typechecks!
})(classtypes || (classtypes = {}));
//# sourceMappingURL=5.classtypesgenerics.js.map