var generics;
(function (generics) {
    function identity(arg) {
        return arg;
    }
    function identity2(arg) {
        return arg;
    }
    function identity3(arg) {
        return arg;
    }
    identity3(10);
    identity3("sample");
    function identity4(arg) {
        return arg;
    }
    function loggingIdentity(arg) {
        //console.log(arg.length);  // Error: T doesn't have .length
        return arg;
    }
    function loggingIdentity2(arg) {
        console.log(arg.length); // Array has a .length, so no more error
        return arg;
    }
})(generics || (generics = {}));
//# sourceMappingURL=1.generics.js.map