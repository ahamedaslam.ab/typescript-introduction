//Extending Internal modules
var App;
(function (App) {
    var Tools;
    (function (Tools) {
        var Utils;
        (function (Utils) {
            Utils.LoggerMode = {
                Console: 1,
                Alert: 2,
            };
            class Logger {
                constructor(mode = Utils.LoggerMode.Console) {
                    this.mode = mode;
                    switch (this.mode) {
                        case Utils.LoggerMode.Console:
                            this.writer = (msg) => console.log(msg);
                            break;
                        case Utils.LoggerMode.Alert:
                            this.writer = (msg) => alert(msg);
                            break;
                    }
                }
                write(msg) {
                    this.writer(msg);
                }
                ;
            }
            Utils.Logger = Logger;
        })(Utils = Tools.Utils || (Tools.Utils = {}));
    })(Tools = App.Tools || (App.Tools = {}));
})(App || (App = {}));
(function (App) {
    var Tools;
    (function (Tools) {
        var Shapes;
        (function (Shapes) {
            class Point {
                constructor(x, y) {
                    this.x = x;
                    this.y = y;
                }
                getDist() { return Math.sqrt(this.x * this.x + this.y * this.y); }
            }
            Shapes.Point = Point;
        })(Shapes = Tools.Shapes || (Tools.Shapes = {}));
    })(Tools = App.Tools || (App.Tools = {}));
})(App || (App = {}));
(function (App) {
    var Tools;
    (function (Tools) {
        var Shapes;
        (function (Shapes) {
            class Rectangle {
                constructor(height, width) {
                    this.height = height;
                    this.width = width;
                }
                getPerimeter() { return this.height * 2 + this.width * 2; }
                getArea() { return this.height * this.width; }
            }
            Shapes.Rectangle = Rectangle;
        })(Shapes = Tools.Shapes || (Tools.Shapes = {}));
    })(Tools = App.Tools || (App.Tools = {}));
})(App || (App = {}));
var Tools = App.Tools;
//Wrapper will pull variables below out of the global scope
(() => {
    var log = new Tools.Utils.Logger(App.Tools.Utils.LoggerMode.Console);
    var p = new Tools.Shapes.Point(3, 4);
    var dist = p.getDist();
    log.write("distance = " + dist);
    var rect = new Tools.Shapes.Rectangle(10, 4);
    var perimeter = rect.getPerimeter();
    log.write("perimeter = " + perimeter);
})();
//# sourceMappingURL=3.extendingmodules.js.map