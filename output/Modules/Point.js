class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    getDist() { return Math.sqrt(this.x * this.x + this.y * this.y); }
}
var p = new Point(3, 4);
var dist = p.getDist();
//# sourceMappingURL=Point.js.map