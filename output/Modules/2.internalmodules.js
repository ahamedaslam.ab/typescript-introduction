var Shapes;
(function (Shapes) {
    class Rectangle {
        constructor(height, width) {
            this.height = height;
            this.width = width;
        }
        getArea() { return this.height * this.width; }
    }
    Shapes.Rectangle = Rectangle;
})(Shapes || (Shapes = {}));
var MyProgram;
(function (MyProgram) {
    function run() {
        var rect = new Shapes.Rectangle(10, 4);
        var area = rect.getArea();
        console.log("area = " + area);
    }
    run();
})(MyProgram || (MyProgram = {}));
//# sourceMappingURL=2.internalmodules.js.map