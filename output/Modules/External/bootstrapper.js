define(["require", "exports", "./alerter"], function (require, exports, alerter_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    function run() {
        var alerter = new alerter_1.Alerter();
        alerter.showMessage();
    }
    exports.run = run;
    ;
});
//# sourceMappingURL=bootstrapper.js.map