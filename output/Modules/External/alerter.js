define(["require", "exports", "./dataservice"], function (require, exports, dataservice_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var dataservice = new dataservice_1.DataService();
    class Alerter {
        constructor() {
            this.name = 'John';
        }
        showMessage() {
            var msg = dataservice.getMessage();
            alert(msg + ', ' + this.name);
        }
        ;
    }
    exports.Alerter = Alerter;
});
//# sourceMappingURL=alerter.js.map