define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    ;
    class DataService {
        constructor() {
            this.msg = 'Welcome to the Show!';
        }
        getMessage() { return this.msg; }
    }
    exports.DataService = DataService;
});
//# sourceMappingURL=dataservice.js.map