define(["require", "exports", "requirejs"], function (require, exports, requirejs_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    requirejs_1.require.config({
        baseUrl: 'output/Modules/External'
    });
    requirejs_1.require(['bootstrapper'], (bootstrapper) => {
        bootstrapper.run();
    });
});
//# sourceMappingURL=main.js.map