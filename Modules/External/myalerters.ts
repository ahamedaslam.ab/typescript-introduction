import { DataService } from './dataservice'

var dataservice = new DataService();

export interface IAlerter {
    name: string;
    showMessage(): void;
}

export class Alerter implements IAlerter {
    name = 'John';
    showMessage() {
        var msg = dataservice.getMessage();
        alert(msg + ', ' + this.name);
    };
}

export class FancyAlerter implements IAlerter{
    name: string;    
    showMessage(): void {
        // to do show some fancy alert box;
    }

    
}