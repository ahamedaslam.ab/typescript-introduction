class Engine {
    constructor(public horsePower: number, public engineType: string) { }
}

class OlderEngine {
    public horsePower: number;
    public engineType: string;

    constructor() {
        let newEngine = new Engine(300, "Single Cylinder");

        let olderEngine = new OlderEngine();
        olderEngine.engineType = "No Cyclinder";
        olderEngine.horsePower = 50;
    }
}


class Car {
    private _engine: Engine;

    constructor(engine: Engine) {
        this.engine = engine;
    }

    get engine(): Engine {
        return this._engine;
    }

    set engine(value: Engine) {
        if (value == undefined) throw 'Please supply an engine';
        this._engine = value;
    }

    start(): void {
        console.log('Car engine started ' + this._engine.engineType);
    }
}


let engine = new Engine(300, 'V8');
let car = new Car(engine);

var i20Engine = new Engine(1000, "i20");
car.engine = i20Engine;
console.log(car.engine.engineType);
car.start();


