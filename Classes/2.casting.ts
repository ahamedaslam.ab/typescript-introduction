class Dog {
    bark() {
        console.log("barked");
    }
}

class Cat {
    yowl() {
        console.log("Yowl")
    }
}

class Me {
    constructor(private pet: Dog | Cat) {

    }

    doTrick() {

        if (this.pet instanceof Dog) {
            //this.pet.bark();
            let dog = <Dog>this.pet;

            dog.bark();
        }
        else {
            
            let cat = <Cat>this.pet;

            cat.yowl();
        }
    }
}